const MongoClient = require('mongodb').MongoClient;
const collection_name = 'billing';
const billing_information = () => {
    return {status : 'OK'}
};

const get_billing =async() =>{
    try {
        const db = await new MongoClient.connect(process.env.MONGODB_URI);
        const collection = db.collection(collection_name);
        return collection.find().toArray();
    }
    catch(err){
        console.log(err);
    }
};

module.exports = {
    information: billing_information(),
    get: get_billing()
};
